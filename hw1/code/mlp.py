import numpy as np


class NeuralNetwork:
    def __init__(self):
        self.layers = []
        self.loss = None
        self.metrics = []

    def add_layer(self, layer):
        # TODO: check layers compatibility
        self.layers.append(layer)

    def assign_loss(self, loss):
        self.loss = loss

    def add_metric(self, metric):
        """Metric is tuple: (name, metric_function)
        """
        self.metrics.append(metric)

    def forward_pass(self, X, y=None):
        for layer in self.layers:
            X = layer(X)
        if y is not None:
            out_dict = {}
            out_dict['loss'] = self.loss(X, y)
            for name, metric in self.metrics:
                out_dict[name] = metric(X, y)
        return out_dict

    def backward_pass(self, X=None, y=None):
        if X is not None:
            if y is None:
                raise ValueError("Specify labels")
            self.forward_pass(X, y)

        g = self.loss.backward()
        for layer in self.layers[-1::-1]:
            g = layer.backward(g)

    def get_gradvars(self, X=None, y=None):
        if X is not None:
            if y is None:
                raise ValueError("Specify labels")
            self.backward_pass(X, y)

        gradvars = []
        for layer in self.layers:
            gradvars.extend(layer.get_gradvars())
        return gradvars


class Layer:
    def __init__(self):
        self.X = None
        self.g = None

    def __call__(self, X):
        self.X = X
        return self._forward()

    def backward(self, g):
        if self.X is None:
            raise ValueError("Forward pass was never completed")
        self.g = g
        return self._backward()

    def get_gradvars(self):
        if self.g is None:
            raise ValueError("Backward pass was never completed")
        return self._get_gradvars()

    def _forward(self, X):
        raise NotImplementedError("Implement me")

    def _backward(self, g):
        raise NotImplementedError("Implement me")

    def _get_gradvars(self):
        raise NotImplementedError("Implement me")


class Loss:
    def __init__(self):
        self.X = None

    def __call__(self, X, y):
        self.X = X
        self.y = y
        return self._forward()

    def backward(self):
        if self.X is None:
            raise ValueError("Forward pass was never completed")
        return self._backward()

    def _forward(self, X):
        raise NotImplementedError("Implement me")

    def _backward(self, g):
        raise NotImplementedError("Implement me")


class FullyConnected(Layer):
    def __init__(self, in_size, out_size, W=None):
        super().__init__()
        b = np.sqrt(6 / (in_size + out_size))
        if W is None:
            self.W = np.random.uniform(-b, b, (in_size, out_size))
        else:
            self.W = W

    def _forward(self):
        return self.X.dot(self.W)

    def _backward(self):
        return self.g.dot(self.W.T)

    def _get_gradvars(self):
        grad = np.mean(np.einsum('ij,ik->ijk', self.X, self.g), axis=0)
        return [(self.W, grad)]


class BatchNorm(Layer):
    def __init__(self, eps=1e-7):
        super().__init__()
        self.eps = eps

    def _forward(self):
        self.mu = np.mean(self.X, axis=0)
        self.std = np.std(self.X, axis=0) + self.eps
        self.X_hat = (self.X - self.mu) / self.std
        return self.X_hat

    def _backward(self):
        g_mu = np.mean(self.g, axis=0)
        gx_mu = np.mean(self.g * self.X_hat, axis=0)
        return (self.g - g_mu) / self.std - \
               (self.X_hat * gx_mu) / (self.std - self.eps)

    def _get_gradvars(self):
        return []


class Bias(Layer):
    def __init__(self, size, b=None):
        super().__init__()
        if b is None:
            self.b = np.zeros(size)
        else:
            self.b = b

    def _forward(self):
        return self.X + self.b

    def _backward(self):
        return self.g

    def _get_gradvars(self):
        grad = np.mean(self.g, axis=0)
        return [(self.b, grad)]


class Scale(Layer):
    def __init__(self, size, gamma=None):
        super().__init__()
        if gamma is None:
            self.gamma = np.ones(size)
        else:
            self.gamma = gamma

    def _forward(self):
        return self.X * self.gamma

    def _backward(self):
        return self.g * self.gamma

    def _get_gradvars(self):
        grad = np.mean(self.g * self.X, axis=0)
        return [(self.gamma, grad)]


class Sigmoid(Layer):
    def __init__(self):
        super().__init__()

    def _forward(self):
        self.out = 1 / (1 + np.exp(-self.X))
        return self.out

    def _backward(self):
        return self.out * (1 - self.out) * self.g

    def _get_gradvars(self):
        return []


class Tanh(Layer):
    def __init__(self):
        super().__init__()

    def _forward(self):
        exp = np.exp(-2 * self.X)
        self.out = (1 - exp) / (1 + exp)
        return self.out

    def _backward(self):
        return (1 - self.out ** 2) * self.g

    def _get_gradvars(self):
        return []


class ReLU(Layer):
    def __init__(self):
        super().__init__()

    def _forward(self):
        self.mask = (self.X > 0).astype(np.float)
        return self.X * self.mask

    def _backward(self):
        return self.g * self.mask

    def _get_gradvars(self):
        return []


class SoftMax(Layer):
    def __init__(self):
        super().__init__()

    def _forward(self):
        X_st = self.X - np.max(self.X, axis=1, keepdims=True)
        sum_exp = np.sum(np.exp(X_st), axis=1, keepdims=True)
        self.out = np.exp(X_st) / sum_exp
        return self.out

    def _backward(self):
        sm_matrix = -np.einsum('ij,ik->ijk', self.out, self.out)
        tmp_matrix = np.zeros(np.shape(sm_matrix)) + np.eye(self.out.shape[-1])
        tmp_matrix = np.einsum('ijk,ij->ijk', tmp_matrix, self.out)
        sm_matrix += tmp_matrix
        return np.einsum('ijk,ik->ij', sm_matrix, self.g)


class CrossEntropy(Loss):
    def __init__(self):
        super().__init__()

    def _forward(self):
        self.p = self.X[np.arange(self.X.shape[0]), self.y]
        return -np.mean(np.log(self.p))

    def _backward(self):
        # Numerically unstable! Use merged with softmax whenever possible
        g = np.zeros(self.X.shape)
        g[np.arange(self.X.shape[0]), self.y] = -1.0 / self.p
        return g


class SoftMaxCrossEntropy(Loss):
    def __init__(self):
        super().__init__()

    def _forward(self):
        self.X_st = self.X - np.max(self.X, axis=1, keepdims=True)
        self.sum_exp = np.sum(np.exp(self.X_st), axis=1, keepdims=True)
        return np.mean(-self.X_st[np.arange(self.X.shape[0]), self.y] + \
                       np.log(self.sum_exp))

    def _backward(self):
        softmax = np.exp(self.X_st) / self.sum_exp
        preds = (np.arange(10)[:,np.newaxis] == self.y).T.astype(np.float)
        return -preds + softmax


def accuracy(X, y):
    return np.mean(np.equal(np.argmax(X, axis=1), y))

