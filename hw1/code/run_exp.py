import argparse
import numpy as np
import os

from train import display_layer, fit_network
from dataset import Dataset
from sgd import MomentumSGD
from mlp import *


def onelayer_network(h=100):
    nn = NeuralNetwork()
    nn.add_layer(FullyConnected(784, h))
    nn.add_layer(Bias(h))
    nn.add_layer(Sigmoid())
    nn.add_layer(FullyConnected(h, 10))
    nn.add_layer(Bias(10))
    nn.assign_loss(SoftMaxCrossEntropy())
    nn.add_metric(('accuracy', accuracy))
    return nn


def twolayer_network(h=100, activ_func=Sigmoid):
    nn = NeuralNetwork()
    nn.add_layer(FullyConnected(784, h))
    nn.add_layer(Bias(h))
    nn.add_layer(activ_func())
    nn.add_layer(FullyConnected(h, h))
    nn.add_layer(Bias(h))
    nn.add_layer(activ_func())
    nn.add_layer(FullyConnected(h, 10))
    nn.add_layer(Bias(10))
    nn.assign_loss(SoftMaxCrossEntropy())
    nn.add_metric(('accuracy', accuracy))
    return nn


def batchnorm_network(h=100):
    nn = NeuralNetwork()
    nn.add_layer(FullyConnected(784, h))
    nn.add_layer(BatchNorm())
    nn.add_layer(Bias(h))
    nn.add_layer(Scale(h))
    nn.add_layer(Sigmoid())
    nn.add_layer(FullyConnected(h, h))
    nn.add_layer(BatchNorm())
    nn.add_layer(Bias(h))
    nn.add_layer(Scale(h))
    nn.add_layer(Sigmoid())
    nn.add_layer(FullyConnected(h, 10))
    nn.add_layer(BatchNorm())
    nn.add_layer(Bias(10))
    nn.add_layer(Scale(10))
    nn.assign_loss(SoftMaxCrossEntropy())
    nn.add_metric(('accuracy', accuracy))
    return nn


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--q', required=True)
    args = parser.parse_args()
    np.random.seed(0)

    if args.q == 'a' or args.q == 'b' or args.q == 'c':
        dirname = 'images/q_abc'
        os.makedirs(dirname, exist_ok=True)
        for i in range(6):
            dataset = Dataset()
            opt = MomentumSGD(momentum=0.0, weight_decay=0.0)
            nn = onelayer_network()
            fit_network(nn, dataset, opt, 32, 200, 0.1, '{}/plot-{}'.format(dirname, i))
            display_layer(nn.layers[0].W, '{}/w-{}'.format(dirname, i))
    elif args.q == 'd':
        dirname = 'images/q_d'
        os.makedirs(dirname, exist_ok=True)
        for lr in [0.1, 0.01, 0.2, 0.5]:
            for m in [0.0, 0.5, 0.9]:
                dataset = Dataset()
                opt = MomentumSGD(momentum=m, weight_decay=0.0)
                nn = onelayer_network()
                fit_network(nn, dataset, opt, 32, 200, lr,
                            '{}/plot-m={}-lr={}'.format(dirname, m, lr).replace(".",","))
                display_layer(nn.layers[0].W,
                              '{}/w-m={}-lr={}'.format(dirname, m, lr).replace(".",","))
    elif args.q == 'e':
        dirname = 'images/q_e'
        os.makedirs(dirname, exist_ok=True)
        for h in [20, 100, 200, 500]:
            dataset = Dataset()
            opt = MomentumSGD(momentum=0.5, weight_decay=0.0)
            nn = onelayer_network(h=h)
            fit_network(nn, dataset, opt, 32, 200, 0.01, '{}/plot-h={}'.format(dirname, h))
            display_layer(nn.layers[0].W, '{}/w-h={}'.format(dirname, h))
    elif args.q == 'f':
        dirname = 'images/q_f'
        os.makedirs(dirname, exist_ok=True)
        for lr in [0.01, 0.1, 0.3, 0.5]:
            for wd in [0.0, 0.001, 0.0001]:
                for h in [100, 200]:
                    dataset = Dataset()
                    opt = MomentumSGD(momentum=0.9, weight_decay=wd)
                    nn = onelayer_network(h)
                    fit_network(nn, dataset, opt, 32, 200, lr,
                                '{}/plot-wd={}-lr={}-h={}-bs=32'.format(dirname, wd, lr, h).replace(".",","))
                    display_layer(nn.layers[0].W,
                                  '{}/w-wd={}-lr={}-h={}-bs=32'.format(dirname, wd, lr, h).replace(".",","))
    elif args.q == 'g':
        dirname = 'images/q_g'
        os.makedirs(dirname, exist_ok=True)
        for lr in [0.01, 0.1, 0.3, 0.5]:
            for wd in [0.0, 0.001, 0.0001, 0.00001]:
                for h in [50, 100, 200, 500]:
                    dataset = Dataset()
                    opt = MomentumSGD(momentum=0.9, weight_decay=wd)
                    nn = twolayer_network(h)
                    fit_network(nn, dataset, opt, 32, 200, lr,
                                '{}/plot-wd={}-lr={}-h={}-bs=32'.format(dirname, wd, lr, h).replace(".",","))
                    display_layer(nn.layers[0].W,
                                  '{}/w-wd={}-lr={}-h={}-bs=32'.format(dirname, wd, lr, h).replace(".",","))
    elif args.q == 'h':
        dirname = 'images/q_h'
        os.makedirs(dirname, exist_ok=True)
        for lr in [0.1, 0.5]:
            for wd in [0.0, 0.0001]:
                for h in [100, 200]:
                    dataset = Dataset()
                    opt = MomentumSGD(momentum=0.9, weight_decay=wd)
                    nn = batchnorm_network(h)
                    fit_network(nn, dataset, opt, 32, 200, lr,
                                '{}/plot-wd={}-lr={}-h={}-bs=32'.format(dirname, wd, lr, h).replace(".",","))
                    display_layer(nn.layers[0].W,
                                  '{}/w-wd={}-lr={}-h={}-bs=32'.format(dirname, wd, lr, h).replace(".",","))
    elif args.q == 'i':
        dirname = 'images/q_i'
        os.makedirs(dirname, exist_ok=True)
        for lr in [0.1, 0.5]:
            for wd in [0.0, 0.0001]:
                for h in [100, 200]:
                    for activ_func in [ReLU, Tanh]:
                        activ_name = 'relu' if activ_func == ReLU else 'tanh'
                        dataset = Dataset()
                        opt = MomentumSGD(momentum=0.9, weight_decay=wd)
                        nn = twolayer_network(h, activ_func=activ_func)
                        fit_network(nn, dataset, opt, 32, 200, lr,
                                    '{}/plot-wd={}-lr={}-h={}-bs=32-activ={}'\
                                    .format(dirname, wd, lr, h, activ_name).replace(".",","))
                        display_layer(nn.layers[0].W,
                                      '{}/w-wd={}-lr={}-h={}-bs=32-activ={}'\
                                      .format(dirname, wd, lr, h, activ_name).replace(".",","))
    elif args.q == 'test':
        dirname = 'images/q_test'
        os.makedirs(dirname, exist_ok=True)
        dataset = Dataset()
        opt = MomentumSGD(momentum=0.9, weight_decay=0.000)
        nn = twolayer_network(activ_func=ReLU)
        fit_network(nn, dataset, opt, 32, 10, 0.2, '{}/plot'.format(dirname))
        display_layer(nn.layers[0].W, '{}/w'.format(dirname))
    else:
        print("Incorrect question number")

