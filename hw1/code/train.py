import numpy as np
import matplotlib.pyplot as plt
import time
import math


def display_layer(x, filename='layer.png'):
    nm = math.ceil(np.sqrt(x.shape[1]))
    sz = 29
    img = np.zeros((nm*sz - 1, nm*sz - 1)) - 0.5
    k = 0
    for i in range(nm):
        if k == x.shape[1]:
            break
        for j in range(nm):
            if k == x.shape[1]:
                break
            img[i*sz:i*sz+28, j*sz:j*sz+28] = x[:, k].reshape(28, 28)
            k += 1

    plt.figure(figsize=(10,10))
    plt.axis('off')
    plt.imshow(img, cmap='gray')
    plt.savefig(filename)


def train_iter(nn, batch_size, lr, dataset, optimizer):
    X_batch, y_batch = dataset.get_next_train_batch(batch_size)
    metrics = nn.forward_pass(X_batch, y_batch)
    nn.backward_pass()
    optimizer.update_weights(lr, nn.get_gradvars())
    return metrics


def update_lr(init_lr, epoch_num, step=30, decay=2):
    if epoch_num < step:
        return init_lr
    return init_lr / (decay * (epoch_num // step))


def fit_network(nn, dataset, opt, batch_size, num_epochs, init_lr, fname):
    epoch_size = dataset.get_batched_training_size(batch_size)
    train_metrics = {'loss': np.zeros(num_epochs)}
    valid_metrics = {'loss': np.zeros(num_epochs)}
    for name, func in nn.metrics:
        train_metrics[name] = np.zeros(num_epochs)
        valid_metrics[name] = np.zeros(num_epochs)

    for cur_epoch in range(num_epochs):
        # cur_lr = update_lr(init_lr, cur_epoch)
        cur_lr = init_lr
        tm = time.time()
        print("Epoch {}/{}".format(cur_epoch + 1, num_epochs), flush=True)

        for it_num in range(epoch_size):
            metrics = train_iter(nn, batch_size, cur_lr, dataset, opt)

            to_print_str = "{}/{} - ".format(it_num + 1, epoch_size)
            to_print_str += ', '.join(['{}: {:.4f}'.format(m_name, m_val)
                                       for m_name, m_val in metrics.items()])
            str_end = "\r" if it_num + 1 != epoch_size else ""
            print(to_print_str, end=str_end, flush=True)

        X_train, y_train = dataset.get_train_data()
        metrics = nn.forward_pass(X_train, y_train)
        for m_name, m_val in metrics.items():
            train_metrics[m_name][cur_epoch] = m_val

        X_valid, y_valid = dataset.get_valid_data()
        metrics = nn.forward_pass(X_valid, y_valid)
        for m_name, m_val in metrics.items():
            valid_metrics[m_name][cur_epoch] = m_val

        to_print_str = " [valid "
        to_print_str += ', '.join(['{}: {:.4f}'.format(m_name, m_val)
                                   for m_name, m_val in metrics.items()])
        to_print_str += ", time: {:.2f}s]".format(time.time() - tm)
        print(to_print_str)

    X_test, y_test = dataset.get_test_data()
    metrics = nn.forward_pass(X_test, y_test)

    to_print_str = ', '.join(['{}: {:.4f}'.format(m_name, m_val)
                              for m_name, m_val in metrics.items()])
    print("Test metrics: {}".format(to_print_str))

    fig, axes = plt.subplots(nrows=1, ncols=2, figsize=(18, 6))
    i = 0
    fsize = 16
    zipped_dict = zip(train_metrics.items(), valid_metrics.items())
    for (tr_name, tr_vals), (vl_name, vl_vals) in zipped_dict:
        axes[i].set_title("Final test {} = {:.4f}".format(tr_name, metrics[tr_name]),
                         {'fontsize': fsize})
        axes[i].set_ylabel(tr_name, {'fontsize': fsize})
        axes[i].set_xlabel('iter', {'fontsize': fsize})
        axes[i].plot(np.arange(tr_vals.shape[0]) + 1, tr_vals, label='train')
        axes[i].plot(np.arange(vl_vals.shape[0]) + 1, vl_vals, label='valid')
        axes[i].legend(fontsize=fsize)
        i += 1
    plt.savefig(fname)

