import numpy as np
import os
from math import ceil
import pandas as pd


def read_file(filename):
    data = pd.read_csv(filename, header=None)
    X = data.iloc[:, :-1].as_matrix()
    y = data.iloc[:, -1].as_matrix()
    return X, y


class Dataset:
    def __init__(self):
        self.X_train, self.y_train = read_file('data/digitstrain.txt')
        self.X_valid, self.y_valid = read_file('data/digitsvalid.txt')
        self.X_test, self.y_test = read_file('data/digitstest.txt')

        self._shuffle_data()
        self.cur_idx = 0

    def preprocess(self, img):
        return img

    def _shuffle_data(self):
        sz = self.X_train.shape[0]
        new_idx = np.random.choice(sz, size=sz, replace=False)
        self.X_train = self.X_train[new_idx]
        self.y_train = self.y_train[new_idx]

    def get_next_train_batch(self, batch_size):
        last_idx = min(self.cur_idx + batch_size, self.X_train.shape[0])
        ret = (
            self.preprocess(self.X_train[self.cur_idx:last_idx]),
            self.y_train[self.cur_idx:last_idx],
        )
        if last_idx == self.X_train.shape[0]:
            self._shuffle_data()
            self.cur_idx = 0
        else:
            self.cur_idx = last_idx
        return ret

    def get_batched_training_size(self, batch_size):
        return ceil(self.X_train.shape[0] / batch_size)

    def get_train_data(self):
        return self.preprocess(self.X_train), self.y_train

    def get_test_data(self):
        return self.preprocess(self.X_test), self.y_test

    def get_valid_data(self):
        return self.preprocess(self.X_valid), self.y_valid

    def move_idx_to_batch(self, batch_num, batch_size):
        self.cur_idx = batch_num * batch_size


class NoiseAutoEncoderDataset(Dataset):
    def noise(self, X):
        drop_mask = (np.random.rand(*X.shape) < 0.1).astype(np.float)
        return X * drop_mask

    def get_next_train_batch(self, batch_size):
        last_idx = min(self.cur_idx + batch_size, self.X_train.shape[0])
        ret = (
            self.noise(self.X_train[self.cur_idx:last_idx]),
            self.preprocess(self.X_train[self.cur_idx:last_idx]),
        )
        if last_idx == self.X_train.shape[0]:
            self._shuffle_data()
            self.cur_idx = 0
        else:
            self.cur_idx = last_idx
        return ret

    def get_train_data(self):
        return self.preprocess(self.X_train), self.preprocess(self.X_train)

    def get_test_data(self):
        return self.preprocess(self.X_test), self.preprocess(self.X_test)

    def get_valid_data(self):
        return self.preprocess(self.X_valid), self.preprocess(self.X_valid)


class AutoEncoderDataset(Dataset):
    def get_next_train_batch(self, batch_size):
        last_idx = min(self.cur_idx + batch_size, self.X_train.shape[0])
        ret = (
            self.preprocess(self.X_train[self.cur_idx:last_idx]),
            self.preprocess(self.X_train[self.cur_idx:last_idx]),
        )
        if last_idx == self.X_train.shape[0]:
            self._shuffle_data()
            self.cur_idx = 0
        else:
            self.cur_idx = last_idx
        return ret

    def get_train_data(self):
        return self.preprocess(self.X_train), self.preprocess(self.X_train)

    def get_test_data(self):
        return self.preprocess(self.X_test), self.preprocess(self.X_test)

    def get_valid_data(self):
        return self.preprocess(self.X_valid), self.preprocess(self.X_valid)

