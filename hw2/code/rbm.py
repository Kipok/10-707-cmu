import numpy as np
import matplotlib.pyplot as plt


def sigmoid(x):
	return 1.0 / (1.0 + np.exp(-x))


class RBM:
  def __init__(self, x_dim=784, h_dim=100):
    self.W = np.random.normal(0, 0.1, (h_dim, x_dim))
    self.b = np.zeros((h_dim))
    self.c = np.zeros((x_dim))

  def get_samples(self, X, k):
    X_sample = X.copy()
    for i in range(k):
      H_probs = sigmoid(X_sample.dot(self.W.T) + self.b)
      H_sample = (H_probs > np.random.rand(*H_probs.shape)).astype(np.float)
      X_probs = sigmoid(H_sample.dot(self.W) + self.c)
      X_sample = (X_probs > np.random.rand(*X_probs.shape)).astype(np.float)
    return X_sample, X_probs

  def update_params(self, X, lr, k):
    X_sample = self.get_samples(X, k)[0]
    H_probs_hat = sigmoid(X_sample.dot(self.W.T) + self.b)
    H_probs = sigmoid(X.dot(self.W.T) + self.b)
    grad_W = (-np.einsum('ij,ik->jk', H_probs, X) +
              np.einsum('ij,ik->jk', H_probs_hat, X_sample)) / X.shape[0]
    grad_b = np.mean(-H_probs + H_probs_hat, axis=0)
    grad_c = np.mean(-X + X_sample, axis=0)

    self.W -= lr * grad_W
    self.b -= lr * grad_b
    self.c -= lr * grad_c

  def cross_entropy_reco(self, X):
    X_prob = self.get_samples(X, 1)[1]
    return -np.mean(np.sum(X * np.log(X_prob) + (1.0 - X) * np.log(1.0 - X_prob), axis=1))

  def free_energy(self, X):
    return np.mean(X.dot(self.c) + np.sum(np.log(1.0 + np.exp(self.b + X.dot(self.W.T))), axis=1))

  def visualize(self, X):
    H_probs = sigmoid(X.dot(self.W.T) + self.b)
    plt.matshow(H_probs, cmap='gray')

  def train(self, dataset, k=1, lr=0.01, epochs=100, batch_size=1):
    epoch_size = dataset.get_batched_training_size(batch_size)
    train_errors = []
    valid_errors = []
    X, _ = dataset.get_train_data()
    train_errors.append(self.cross_entropy_reco(X))
    X, _ = dataset.get_valid_data()
    valid_errors.append(self.cross_entropy_reco(X))
    for epoch_num in range(epochs):
      print("Epoch #{}".format(epoch_num))
      for it in range(epoch_size):
        X, _ = dataset.get_next_train_batch(batch_size)
        self.update_params(X, lr, k)
      X, _ = dataset.get_train_data()
      train_error = self.cross_entropy_reco(X)
      train_energy = self.free_energy(X)
      X, _ = dataset.get_valid_data()
      valid_error = self.cross_entropy_reco(X)
      valid_energy = self.free_energy(X)
      print("Train error: {:.4f}, valid error: {:.4f}, energy diff: {:.4f}"\
            .format(train_error, valid_error, train_energy - valid_energy))
      train_errors.append(train_error)
      valid_errors.append(valid_error)
    return train_errors, valid_errors

