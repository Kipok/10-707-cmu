import argparse
import numpy as np
import os
import math

from train import fit_network
from dataset import Dataset, AutoEncoderDataset, NoiseAutoEncoderDataset
from sgd import MomentumSGD
from mlp import *
from rbm import RBM
import matplotlib.pyplot as plt


def display_layer(x, filename='layer.png'):
  nm = math.ceil(np.sqrt(x.shape[1]))
  sz = 29
  img = np.zeros((nm*sz - 1, nm*sz - 1)) - 0.5
  k = 0
  for i in range(nm):
    if k == x.shape[1]:
      break
    for j in range(nm):
      if k == x.shape[1]:
        break
      img[i*sz:i*sz+28, j*sz:j*sz+28] = x[:, k].reshape(28, 28)
      k += 1

  plt.figure(figsize=(10,10))
  plt.axis('off')
  plt.imshow(img, cmap='gray')
  plt.savefig(filename)


def onelayer_network(W, b, h=100):
  nn = NeuralNetwork()
  nn.add_layer(FullyConnected(784, h, W=W))
  nn.add_layer(Bias(h, b=b))
  nn.add_layer(Sigmoid())
  nn.add_layer(FullyConnected(h, 10))
  nn.add_layer(Bias(10))
  nn.assign_loss(SoftMaxCrossEntropy())
  nn.add_metric(('accuracy', accuracy))
  return nn


def autoencoder(h=100):
  W1 = np.random.normal(0.0, 0.1, size=(784, h))
  W2 = np.random.normal(0.0, 0.1, size=(h, 784))
  nn = NeuralNetwork()
  nn.add_layer(FullyConnected(784, h, W=W1))
  nn.add_layer(Bias(h))
  nn.add_layer(Sigmoid())
  nn.add_layer(FullyConnected(h, 784, W=W2))
  nn.add_layer(Bias(784))
  nn.assign_loss(SigmoidCrossEntropy())
  return nn


def save_plot(name, tr_errors, vl_errors):
  plt.figure()
  plt.plot(np.arange(len(tr_errors)-1)+1, tr_errors[1:], label='training error')
  plt.plot(np.arange(len(tr_errors)-1)+1, vl_errors[1:], label='validation error')
  plt.legend()
  plt.title("RBM training")
  plt.savefig('{}'.format(name))


def run_classification(name, W, b):
  dataset = Dataset()
  opt = MomentumSGD(momentum=0.9, weight_decay=0.0001)
  nn = onelayer_network(W, b)
  fit_network(nn, dataset, opt, 32, 200, 0.1, name)


if __name__ == '__main__':
  parser = argparse.ArgumentParser()
  parser.add_argument('--q', required=True)
  args = parser.parse_args()
  np.random.seed(0)

  if args.q == 'a':
    dirname = 'images/q_a'
    os.makedirs(dirname, exist_ok=True)
    epochs_list = [0, 15, 30, 50, 100, 200, 500]
    for bs in [1, 10]:
      for lr in [0.01, 0.05, 0.1]:
        k = 1
        dataset = Dataset()
        rbm = RBM()
        for i in range(len(epochs_list) - 1):
          epochs = epochs_list[i + 1] - epochs_list[i]
          tr_errors, vl_errors = rbm.train(dataset, k=k, epochs=epochs, batch_size=bs, lr=lr)
          pm_name = 'k={}-bs={}-lr={}-epochs={}'.format(k, bs, lr, epochs).replace(".",",")
          display_layer(rbm.W.T, '{}/weights-{}.png'.format(dirname, pm_name))
          save_plot('{}/plot-{}.pdf'.format(dirname, pm_name), tr_errors, vl_errors)
          samples, sample_probs = rbm.get_samples(np.random.rand(100, 784), k=1000)
          display_layer(sample_probs.T, '{}/samples-1000-{}.png'.format(dirname, pm_name))
          samples, sample_probs = rbm.get_samples(np.random.rand(100, 784), k=10000)
          display_layer(sample_probs.T, '{}/samples-10000-{}.png'.format(dirname, pm_name))
  if args.q == 'b' or args.q == 'c':
    dirname = 'images/q_bc'
    os.makedirs(dirname, exist_ok=True)
    epochs_list = [0, 15, 30, 50, 100, 200]
    for k in [5, 10, 20]:
      for lr in [0.01, 0.1]:
        bs = 10
        dataset = Dataset()
        rbm = RBM()
        for i in range(len(epochs_list) - 1):
          epochs = epochs_list[i + 1] - epochs_list[i]
          tr_errors, vl_errors = rbm.train(dataset, k=k, epochs=epochs, batch_size=bs, lr=lr)
          pm_name = 'k={}-bs={}-lr={}-epochs={}'.format(k, bs, lr, epochs_list[i+1]).replace(".",",")
          display_layer(rbm.W.T, '{}/weights-{}.png'.format(dirname, pm_name))
          save_plot('{}/plot-{}.pdf'.format(dirname, pm_name), tr_errors, vl_errors)
          samples, sample_probs = rbm.get_samples(np.random.rand(100, 784), k=1000)
          display_layer(sample_probs.T, '{}/samples-1000-{}.png'.format(dirname, pm_name))
  elif args.q == 'd':
    dirname = 'images/q_d'
    os.makedirs(dirname, exist_ok=True)
    dataset = Dataset()
    rbm = RBM()
    tr_errors, vl_errors = rbm.train(dataset, k=1, epochs=200, batch_size=1, lr=0.1)
    run_classification('{}/classification.pdf'.format(dirname), rbm.W.T, rbm.b)
  elif args.q == 'e':
    dirname = 'images/q_e'
    os.makedirs(dirname, exist_ok=True)
    dataset = AutoEncoderDataset()
    opt = MomentumSGD(momentum=0.9, weight_decay=0.01)
    nn = autoencoder()
    fit_network(nn, dataset, opt, 32, 100, 0.1, "{}/encoder.pdf".format(dirname))
    display_layer(nn.layers[0].W, '{}/weights.png'.format(dirname))
    run_classification('{}/classification.pdf'.format(dirname), nn.layers[0].W, nn.layers[1].b)
  elif args.q == 'f':
    dirname = 'images/q_f'
    os.makedirs(dirname, exist_ok=True)
    dataset = NoiseAutoEncoderDataset()
    opt = MomentumSGD(momentum=0.9, weight_decay=0.0001)
    nn = autoencoder()
    fit_network(nn, dataset, opt, 32, 100, 0.1, "{}/encoder.pdf".format(dirname))
    display_layer(nn.layers[0].W, '{}/weights.png'.format(dirname))
    run_classification('{}/classification.pdf'.format(dirname), nn.layers[0].W, nn.layers[1].b)
  elif args.q == 'g':
    dirname = 'images/q_g'
    os.makedirs(dirname, exist_ok=True)
    for h in [50, 100, 200, 500]:
      dataset = AutoEncoderDataset()
      opt = MomentumSGD(momentum=0.9, weight_decay=0.01)
      nn = autoencoder(h)
      fit_network(nn, dataset, opt, 32, 100, 0.1, "{}/encoder-h={}.pdf".format(dirname, h))
      display_layer(nn.layers[0].W, '{}/auto_weights-h={}.png'.format(dirname, h))
    for h in [50, 100, 200, 500]:
      dataset = NoiseAutoEncoderDataset()
      opt = MomentumSGD(momentum=0.9, weight_decay=0.0001)
      nn = autoencoder(h)
      fit_network(nn, dataset, opt, 32, 100, 0.1, "{}/noise_encoder-h={}.pdf".format(dirname, h))
      display_layer(nn.layers[0].W, '{}/noise_weights-h={}.png'.format(dirname, h))
    for h in [50, 100, 200, 500]:
      dataset = Dataset()
      rbm = RBM(h_dim=h)
      tr_errors, vl_errors = rbm.train(dataset, k=10, epochs=100, batch_size=10, lr=0.01)
      display_layer(rbm.W.T, '{}/rbm-weights-h={}.png'.format(dirname, h))
      save_plot('{}/rbm-plot-h={}.pdf'.format(dirname, h), tr_errors, vl_errors)
      samples, sample_probs = rbm.get_samples(np.random.rand(100, 784), k=1000)
      display_layer(sample_probs.T, '{}/rbm-samples-1000-h={}.png'.format(dirname, h))
  else:
    print("Incorrect question number")

