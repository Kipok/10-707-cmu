import numpy as np


class MomentumSGD:
    def __init__(self, momentum=0.9, weight_decay=0.0001):
        self.m_coef = momentum
        self.wd = weight_decay
        self.m_vars = None

    def update_weights(self, lr, grad_vars):
        if self.m_vars is None:
            self.m_vars = []
            for var, grad in grad_vars:
                self.m_vars.append(np.zeros(var.shape))

        for i, (var, grad) in enumerate(grad_vars):
            wd_grad = grad + self.wd * var
            self.m_vars[i] = self.m_vars[i] * self.m_coef + wd_grad
            var -= lr * self.m_vars[i]

