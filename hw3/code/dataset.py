import numpy as np
import os
from math import ceil
import pandas as pd
from collections import Counter
import matplotlib.pyplot as plt


def read_data(verbose=False):
  with open('data/train.txt', 'rt') as fin:
    lines = fin.readlines()

  with open('data/val.txt', 'rt') as fin:
    lines_val = fin.readlines()

  word_freq = dict()
  for line in lines:
    for word in line.lower().split():
      if word in word_freq:
        word_freq[word] += 1
      else:
        word_freq[word] = 1
  word_freq = dict([tup for i, tup in enumerate(
    sorted(word_freq.items(), key=lambda x: -x[1])) if i < 7997])

  word2idx = dict()
  idx2word = dict()
  idx = 0

  for word in ['UNK', 'START', 'END']:
    word2idx[word] = idx
    idx2word[idx] = word
    idx += 1

  for word, freq in word_freq.items():
    word2idx[word] = idx
    idx2word[idx] = word
    idx += 1

  n = 4
  ngrams = []
  for line in lines:
    line_c = ['START'] + line.lower().split() + ['END']
    for i in range(len(line_c) - 3):
      ngrams.append(tuple(word2idx.get(line_c[i + j], 0) for j in range(n)))

  if verbose:
    counts = sorted(Counter(ngrams).items(), key=lambda x: -x[1])
    top_50 = list(map(lambda x: [idx2word[y] for y in x], [cnt[0] for cnt in counts[:50]]))
    print("\n\n".join([" ".join(ngram) for ngram in top_50]))
    counts_only = [cnt for ngram, cnt in counts]
    plt.title("n-grams distribution")
    plt.xlabel("n-gram number")
    plt.ylabel("n-gram count")
    plt.plot(np.arange(len(counts_only)), counts_only)
    plt.savefig('images/distr.pdf', bbox_inches='tight')

  ngrams_train = np.array(ngrams)

  n = 4
  ngrams = []
  for line in lines_val:
    line_c = ['START'] + line.split() + ['END']
    for i in range(len(line_c) - 3):
      ngrams.append(tuple(word2idx.get(line_c[i + j], 0) for j in range(n)))

  ngrams_test = np.array(ngrams)
  return (ngrams_train[:,:3], ngrams_train[:,3],
          ngrams_test[:,:3], ngrams_test[:,3],
          word2idx, idx2word)


def onehot(y, sz=8000):
  return (np.arange(sz)[:,np.newaxis] == y).T


class Dataset:
  def __init__(self):
    X_train, y_train, X_valid, y_valid, w2i, i2w = read_data()
    X_test, y_test = X_valid, y_valid

    self.word2idx = w2i
    self.idx2word = i2w

    self.X_data = {}
    self.X_data['train'] = X_train
    self.X_data['test'] = X_test
    self.X_data['valid'] = X_valid

    self.y_data = {}
    self.y_data['train'] = y_train
    self.y_data['test'] = y_test
    self.y_data['valid'] = y_valid

    self._shuffle_data()
    self.cur_idx = {}
    self.cur_idx['train'] = 0
    self.cur_idx['test'] = 0
    self.cur_idx['valid'] = 0

  def preprocess_x(self, X):
    return X

  def preprocess_y(self, y):
    return y # return onehot(y)

  def _shuffle_data(self, set_name='train'):
    sz = self.X_data[set_name].shape[0]
    new_idx = np.random.choice(sz, size=sz, replace=False)
    self.X_data[set_name] = self.X_data[set_name][new_idx]
    self.y_data[set_name] = self.y_data[set_name][new_idx]

  def get_next_batch(self, batch_size, set_name):
    last_idx = min(self.cur_idx[set_name] + batch_size,
                   self.X_data[set_name].shape[0])
    ret = (
      self.preprocess_x(self.X_data[set_name][self.cur_idx[set_name]:last_idx]),
      self.preprocess_y(self.y_data[set_name][self.cur_idx[set_name]:last_idx]),
    )
    if last_idx == self.X_data[set_name].shape[0]:
      if set_name == 'train':
        self._shuffle_data(set_name)
      self.cur_idx[set_name] = 0
    else:
      self.cur_idx[set_name] = last_idx
    return ret

  def get_batched_size(self, batch_size, set_name):
    return ceil(self.X_data[set_name].shape[0] / batch_size)

  def get_data(self, set_name, batch_size=None):
    if batch_size is None:
      return (self.preprocess_x(self.X_data[set_name]),
              self.preprocess_y(self.y_data[set_name]))
    else:
      old_idx = self.cur_idx[set_name]
      self.cur_idx[set_name] = 0
      size = self.get_batched_size(batch_size, set_name)
      for i in range(size):
        yield self.get_next_batch(batch_size, set_name)

      self.cur_idx[set_name] = old_idx

  def move_idx_to_batch(self, batch_num, batch_size, set_name):
    self.cur_idx[set_name] = batch_num * batch_size

