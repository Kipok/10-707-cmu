import argparse
import numpy as np
import os

from train import fit_network
from dataset import Dataset, read_data
from sgd import MomentumSGD
from mlp import *
import matplotlib.pyplot as plt

import torch
import torch.nn.functional as F


def ngram_network(h=128, vocab_size=8000, n_gram=4,
                  emb_dim=16, use_tanh=False):
  emb_size = (n_gram-1) * emb_dim
  nn = NeuralNetwork()
  nn.add_layer(EmbeddingLookup(vocab_size, emb_dim))
  nn.add_layer(FullyConnected(emb_size, h))
  nn.add_layer(Bias(h))
  if use_tanh:
    nn.add_layer(Tanh())
  nn.add_layer(FullyConnected(h, vocab_size))
  nn.add_layer(Bias(vocab_size))
  nn.assign_loss(SoftMaxCrossEntropy(num_labels=vocab_size))
  return nn


def predict_next(ngram, nn, dataset, n=10):
  print(" ".join(ngram), end=" ")
  ngram_idx = np.array([dataset.word2idx[word] for word in ngram])[np.newaxis,:]
  for i in range(n):
    out = nn.forward_pass(ngram_idx)
    next_id = np.argmax(out, axis=1)[0]
    print(dataset.idx2word[next_id], end=" ")
    if next_id == 2:
      break
    ngram_idx[0,:2] = ngram_idx[0,1:]
    ngram_idx[0, 2] = next_id


def embedding_distance(nn, id1, id2):
  return np.linalg.norm(nn.layers[0].W[id1] - nn.layers[0].W[id2])


def visualize_embeddings(nn, dataset, size=500, name='emb.pdf'):
    indices = np.random.choice(8000, size=size, replace=False)
    xs = nn.layers[0].W[indices, 0]
    ys = nn.layers[0].W[indices, 1]

    plt.figure(figsize=(15,15))
    plt.scatter(xs, ys)
    words = [dataset.idx2word[idx] for idx in indices]
    for i, word in enumerate(words):
        plt.annotate(word, xy = (xs[i], ys[i]), xytext = (3, 3),
                     textcoords = 'offset points', ha = 'left', va = 'top')
    plt.savefig(name, bbox_inches='tight')


class NgramRNN(torch.nn.Module):
  def __init__(self, emb_dim, h_dim, vocab_size, truncate=False):
    super(NgramRNN, self).__init__()
    self.embedding_layer = torch.nn.Embedding(vocab_size, emb_dim)
    self.rnn = torch.nn.RNN(emb_dim, h_dim)
    self.hid2out = torch.nn.Linear(h_dim, vocab_size)
    self.truncate = truncate

  def forward(self, nn_input):
    emb_input = self.embedding_layer(nn_input)
    if self.truncate and np.random.rand() < 0.1:
      p_input = emb_input.permute(1, 0, 2)
      rnn_out, rnn_hid = self.rnn(p_input[:1])
      rnn_hid.detach_()
      rnn_out, rnn_hid = self.rnn(p_input[1:], rnn_hid)
    else:
      rnn_out, rnn_hid = self.rnn(emb_input.permute(1, 0, 2))
    out = self.hid2out(rnn_hid.squeeze())
    out_log_probs = F.log_softmax(out)
    return out_log_probs


if __name__ == '__main__':
  parser = argparse.ArgumentParser()
  parser.add_argument('--q', required=True)
  args = parser.parse_args()
  torch.manual_seed(0)
  np.random.seed(0)

  if args.q == '1' or args.q == 'all':
    dirname = 'images'
    os.makedirs(dirname, exist_ok=True)
    data = read_data(verbose=True)
  if args.q == '2' or args.q == 'all':
    dirname = 'images/q_2'
    os.makedirs(dirname, exist_ok=True)
    for h in [128, 256, 512]:
      dataset = Dataset()
      opt = MomentumSGD(momentum=0.9, weight_decay=0.0)
      nn = ngram_network(h=h)
      fit_network(nn, dataset, opt, 512, 100, 0.1, "{}/results-h{}.pdf".format(dirname, h))
  if args.q == '3' or args.q == 'all':
    dirname = 'images/q_3'
    os.makedirs(dirname, exist_ok=True)
    for h in [128, 256, 512]:
      dataset = Dataset()
      opt = MomentumSGD(momentum=0.9, weight_decay=0.0)
      nn = ngram_network(h=h, use_tanh=True)
      fit_network(nn, dataset, opt, 512, 100, 0.1, "{}/results-h{}.pdf".format(dirname, h))
  if args.q == '4' or args.q == 'all':
    dirname = 'images/q_4'
    os.makedirs(dirname, exist_ok=True)
    dataset = Dataset()
    opt = MomentumSGD(momentum=0.9, weight_decay=0.0)
    nn = ngram_network(use_tanh=True)
    fit_network(nn, dataset, opt, 512, 20, 0.1, 'name', plot=False)
    ngrams = [
      ['city', 'of', 'new'],
      ['life', 'in', 'the'],
      ['he', 'is', 'the'],
      ['they', 'are', 'not'],
      ['the', 'company', 'said']
    ]
    for ngram in ngrams:
      predict_next(ngram, nn, dataset)
      print()

    word_pairs = [
      ['city', 'town'],
      ['city', 'they'],
      ['company', 'business'],
      ['company', 'he']
    ]
    for word_pair in word_pairs:
      dst = embedding_distance(nn, dataset.word2idx[word_pair[0]],
                               dataset.word2idx[word_pair[1]])
      print('Distance for words {}, {} = {:.4f}'\
            .format(word_pair[0], word_pair[1], dst))
  if args.q == '5' or args.q == 'all':
    dirname = 'images/q_5'
    os.makedirs(dirname, exist_ok=True)
    dataset = Dataset()
    opt = MomentumSGD(momentum=0.9, weight_decay=0.0)
    nn = ngram_network(emb_dim=2, use_tanh=True)
    fit_network(nn, dataset, opt, 512, 20, 0.1, 'name', plot=False)
    visualize_embeddings(nn, dataset, 500, '{}/emb-500.pdf'.format(dirname))
    visualize_embeddings(nn, dataset, 50, '{}/emb-50.pdf'.format(dirname))
  if args.q == '61' or args.q == 'all':
    dirname = 'images/q_61'
    os.makedirs(dirname, exist_ok=True)
    dataset = Dataset()
    nn = NgramRNN(16, 128, 8000)
    opt = torch.optim.SGD(nn.parameters(), lr=0, momentum=0.9)
    fit_network(nn, dataset, opt, 16, 20, 0.01, '{}/results.pdf'.format(dirname))
  if args.q == '62' or args.q == 'all':
    dirname = 'images/q_62'
    os.makedirs(dirname, exist_ok=True)
    for emb_dim in [32, 64, 128]:
      dataset = Dataset()
      nn = NgramRNN(emb_dim, 128, 8000)
      opt = torch.optim.SGD(nn.parameters(), lr=0, momentum=0.9)
      fit_network(nn, dataset, opt, 16, 20, 0.01,
                  '{}/results-{}.pdf'.format(dirname, emb_dim))
  if args.q == '63' or args.q == 'all':
    dirname = 'images/q_63'
    os.makedirs(dirname, exist_ok=True)
    dataset = Dataset()
    nn = NgramRNN(16, 128, 8000, truncate=True)
    opt = torch.optim.SGD(nn.parameters(), lr=0, momentum=0.9)
    fit_network(nn, dataset, opt, 16, 20, 0.01, '{}/results.pdf'.format(dirname))

