import unittest
import numpy.testing as npt
import numpy as np
import mlp

mlp.FLOAT = np.float64
from mlp import *


FLOAT = np.float64
INT = np.int32


class TestBinaryCrossEntropy(unittest.TestCase):
    def setUp(self):
        self.X = np.random.rand(50, 10).astype(FLOAT)
        self.y = np.random.rand(50, 10).astype(FLOAT)

    def test_merged_cross_entropy(self):
        sm = Sigmoid()
        cs = BinaryCrossEntropy()
        out1 = cs(sm(self.X), self.y)
        back1 = sm.backward(cs.backward())

        smcs = SigmoidCrossEntropy()
        out2 = smcs(self.X, self.y)
        back2 = smcs.backward()

        npt.assert_allclose(out1, out2, atol=1e-5)
        npt.assert_allclose(back1, back2, atol=1e-5)


class TestCrossEntropy(unittest.TestCase):
    def setUp(self):
        self.X = np.random.rand(50, 10).astype(FLOAT)
        self.y = np.random.choice(10, 50).astype(INT)
#        self.y = (np.arange(10)[:,np.newaxis] == self.y).T

    def test_merged_cross_entropy(self):
        sm = SoftMax()
        cs = CrossEntropy()
        out1 = cs(sm(self.X), self.y)
        back1 = sm.backward(cs.backward())

        smcs = SoftMaxCrossEntropy()
        out2 = smcs(self.X, self.y)
        back2 = smcs.backward()

        npt.assert_allclose(out1, out2)
        npt.assert_allclose(back1, back2)


class TestGradients(unittest.TestCase):
    def setUp(self):
        self.nn = NeuralNetwork()
        self.nn.assign_loss(SoftMaxCrossEntropy())
        self.X = np.random.rand(20, 100).astype(FLOAT)
        self.y = np.random.choice(10, 20).astype(INT)
 #       self.y = (np.arange(10)[:,np.newaxis] == self.y).T

    def diff_grad(self, var, eps=1e-5):
        d_grad = np.empty(var.shape, dtype=FLOAT)
        for idx in np.ndindex(d_grad.shape):
            var[idx] += eps
            l1 = self.nn.forward_pass(self.X, self.y)['loss']
            var[idx] -= 2 * eps
            l2 = self.nn.forward_pass(self.X, self.y)['loss']
            var[idx] += eps
            d_grad[idx] = (l1 - l2) / (2 * eps)
        return d_grad

    def grad_test(self):
        gradvars = self.nn.get_gradvars(self.X, self.y)
        for var, grad in gradvars:
            d_grad = self.diff_grad(var)
            npt.assert_allclose(d_grad, grad, atol=1e-5)

    def test_ngram_model(self):
        vocab_size = 100
        ngram_num = 4
        b_size = 10

        self.X = np.random.choice(vocab_size, size=(b_size, ngram_num-1))
        self.y = np.random.choice(vocab_size, size=b_size).astype(INT)
  #      self.y = (np.arange(vocab_size)[:,np.newaxis] == self.y).T

        self.nn = NeuralNetwork()
        self.nn.add_layer(EmbeddingLookup(vocab_size, 16))
        self.nn.add_layer(FullyConnected(16*3, 100))
        self.nn.add_layer(Bias(100))
        self.nn.add_layer(Sigmoid())
        self.nn.add_layer(FullyConnected(100, vocab_size))
        self.nn.add_layer(Bias(vocab_size))
        self.nn.assign_loss(SoftMaxCrossEntropy(num_labels=vocab_size))
        self.grad_test()

    def test_autoencoder(self):
        self.X = np.random.rand(20, 100).astype(FLOAT)
        self.y = np.random.rand(20, 100).astype(FLOAT)
        self.nn = NeuralNetwork()
        self.nn.add_layer(FullyConnected(100, 50))
        self.nn.add_layer(Bias(50))
        self.nn.add_layer(Sigmoid())
        self.nn.add_layer(FullyConnected(50, 100))
        self.nn.add_layer(Bias(100))
        self.nn.assign_loss(SigmoidCrossEntropy())
        self.grad_test()

    def test_fully_connected(self):
        self.nn.add_layer(FullyConnected(100, 50))
        self.nn.add_layer(FullyConnected(50, 10))
        self.grad_test()

    def test_bias(self):
        self.nn.add_layer(FullyConnected(100, 50))
        self.nn.add_layer(Bias(50))
        self.nn.add_layer(FullyConnected(50, 10))
        self.nn.add_layer(Bias(10))
        self.grad_test()

    def test_scale(self):
        self.nn.add_layer(FullyConnected(100, 50))
        self.nn.add_layer(Scale(50))
        self.nn.add_layer(FullyConnected(50, 10))
        self.nn.add_layer(Scale(10))
        self.grad_test()

    def test_batchnorm(self):
        self.nn.add_layer(FullyConnected(100, 50))
        self.nn.add_layer(BatchNorm(50))
        self.nn.add_layer(Bias(50))
        self.nn.add_layer(Scale(50))
        self.nn.add_layer(FullyConnected(50, 10))
        self.nn.add_layer(BatchNorm(10))
        self.nn.add_layer(Bias(10))
        self.nn.add_layer(Scale(10))
        self.grad_test()

    def test_sigmoid(self):
        self.nn.add_layer(FullyConnected(100, 50))
        self.nn.add_layer(Sigmoid())
        self.nn.add_layer(FullyConnected(50, 20))
        self.nn.add_layer(Sigmoid())
        self.nn.add_layer(FullyConnected(20, 10))
        self.grad_test()

    def test_tanh(self):
        self.nn.add_layer(FullyConnected(100, 50))
        self.nn.add_layer(Tanh())
        self.nn.add_layer(FullyConnected(50, 20))
        self.nn.add_layer(Tanh())
        self.nn.add_layer(FullyConnected(20, 10))
        self.grad_test()

    def test_relu(self):
        self.nn.add_layer(FullyConnected(100, 50))
        self.nn.add_layer(ReLU())
        self.nn.add_layer(FullyConnected(50, 20))
        self.nn.add_layer(ReLU())
        self.nn.add_layer(FullyConnected(20, 10))
        self.grad_test()


if __name__ == '__main__':
    unittest.main()

