import numpy as np
import matplotlib.pyplot as plt
import time
import math

import torch
from torch.autograd import Variable

def train_iter(nn, batch_size, lr, dataset, optimizer):
  X_batch, y_batch = dataset.get_next_batch(batch_size, 'train')
  try:
    metrics = nn.forward_pass(X_batch, y_batch)
    nn.backward_pass()
    optimizer.update_weights(lr, nn.get_gradvars())
  except:
    for param_group in optimizer.param_groups:
      param_group['lr'] = lr
    metrics = {'loss': 0}
    nn.zero_grad()
    out_probs = nn(Variable(torch.LongTensor(X_batch)))
    loss_function = torch.nn.NLLLoss()
    loss = loss_function(out_probs, Variable(torch.LongTensor(y_batch)))
    metrics['loss'] = loss.data[0]
    loss.backward()
    optimizer.step()
  return metrics


def update_lr(init_lr, epoch_num, step=6, decay=2):
  if epoch_num < step:
    return init_lr
  return init_lr / (decay * (epoch_num // step))


def get_metrics(nn, dataset, set_name):
  metrics_vals = {'loss': []}
  try:
    for name, func in nn.metrics:
      metrics_vals[name] = []

    for X_batch, y_batch in dataset.get_data(set_name, 512):
      metrics = nn.forward_pass(X_batch, y_batch)
      for m_name, m_val in metrics.items():
        metrics_vals[m_name].append(m_val)
  except:
    for X_batch, y_batch in dataset.get_data(set_name, 512):
      out_probs = nn(Variable(torch.LongTensor(X_batch)))
      loss_function = torch.nn.NLLLoss()
      loss = loss_function(out_probs, Variable(torch.LongTensor(y_batch)))
      metrics_vals['loss'] = loss.data[0]
  return metrics_vals


def fit_network(nn, dataset, opt, batch_size, num_epochs, init_lr, fname, plot=True):
  epoch_size = dataset.get_batched_size(batch_size, 'train')
  train_metrics = {'loss': np.zeros(num_epochs), 'perplexity': np.zeros(num_epochs)}
  valid_metrics = {'loss': np.zeros(num_epochs), 'perplexity': np.zeros(num_epochs)}
#  for name, func in nn.metrics:
#    train_metrics[name] = np.zeros(num_epochs)
#    valid_metrics[name] = np.zeros(num_epochs)

  for cur_epoch in range(num_epochs):
    cur_lr = update_lr(init_lr, cur_epoch)
    cur_lr = init_lr
    tm = time.time()
    print("Epoch {}/{}".format(cur_epoch + 1, num_epochs), flush=True)

    for it_num in range(epoch_size):
      metrics = train_iter(nn, batch_size, cur_lr, dataset, opt)
      for m_name, m_val in metrics.items():
        train_metrics[m_name][cur_epoch] += m_val

      to_print_str = "{}/{} - ".format(it_num + 1, epoch_size)
      to_print_str += ', '.join(['{}: {:.4f}'.format(m_name, m_val)
                                 for m_name, m_val in metrics.items()])
      str_end = "\r" if it_num + 1 != epoch_size else ""
      print(to_print_str, end=str_end, flush=True)

    for m_name, m_val in metrics.items():
      train_metrics[m_name][cur_epoch] /= epoch_size
    train_metrics['perplexity'] = 2 ** (train_metrics['loss'] / np.log(2))

    metrics_vals = get_metrics(nn, dataset, 'valid')
    for m_name, m_val in metrics_vals.items():
      valid_metrics[m_name][cur_epoch] = np.mean(m_val)
    valid_metrics['perplexity'] = 2 ** (valid_metrics['loss'] / np.log(2))

    to_print_str = " [valid "
    to_print_str += ', '.join(['{}: {:.4f}'.format(m_name, m_val[cur_epoch])
                               for m_name, m_val in valid_metrics.items()])
    to_print_str += ", time: {:.2f}s]".format(time.time() - tm)
    print(to_print_str)

  if plot:
    fig, axes = plt.subplots(nrows=1, ncols=len(train_metrics), figsize=(18, 6))
    if len(train_metrics) == 1:
        axes = [axes]
    i = 0
    fsize = 16
    zipped_dict = zip(train_metrics.items(), valid_metrics.items())
    for (tr_name, tr_vals), (vl_name, vl_vals) in zipped_dict:
#      axes[i].set_title("Final test {}".format(tr_name),
#                       {'fontsize': fsize})
      axes[i].set_ylabel(tr_name, {'fontsize': fsize})
      axes[i].set_xlabel('iter', {'fontsize': fsize})
      axes[i].plot(np.arange(tr_vals.shape[0]) + 1, tr_vals, label='train')
      axes[i].plot(np.arange(vl_vals.shape[0]) + 1, vl_vals, label='valid')
      axes[i].legend(fontsize=fsize)
      i += 1
    plt.savefig(fname)

